import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Header from '@/components/Header.vue';

describe('Header.vue', () => {
  it('Should have a img in header', () => {
    const wrapper = shallowMount(Header);
    expect(wrapper.contains('img')).to.equal(true);
  });
});
