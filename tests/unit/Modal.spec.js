import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Modal from '@/components/Modal.vue';

describe('Modal.vue', () => {
  it('Should show compra confirmada', () => {
    const wrapper = shallowMount(Modal, {
      propsData: {
        type: 1,
        show: true,
      },
    });
    expect(wrapper.props().type).to.equal(1);
    expect(wrapper.props().show).to.equal(true);
    expect(wrapper.findAll('.title').at(0).text()).to.equal('compra confirmada');
  });
  it('Should show compra cancelada', () => {
    const wrapper = shallowMount(Modal, {
      propsData: {
        type: 2,
        show: true,
      },
    });

    expect(wrapper.props().type).to.equal(2);
    expect(wrapper.props().show).to.equal(true);
    expect(wrapper.findAll('.title').at(0).text()).to.equal('compra cancelada');
  });
});
